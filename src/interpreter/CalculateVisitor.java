package interpreter;

import SymbolTable.GlobalSymbols;
import grammar.*;
import java.util.Objects;
import java.util.Objects;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.misc.Interval;

public class CalculateVisitor extends firstBaseVisitor<Integer> {

    private TokenStream tokStream = null;
    private CharStream input=null;

    private GlobalSymbols<Integer> symbols = new GlobalSymbols<>();;

    public CalculateVisitor(CharStream inp) {
        super();
        this.input = inp;
    }

    public CalculateVisitor(TokenStream tok) {
        super();
        this.tokStream = tok;
    }
    public CalculateVisitor(CharStream inp, TokenStream tok) {
        super();
        this.input = inp;
        this.tokStream = tok;
    }
    private String getText(ParserRuleContext ctx) {
        int a = ctx.start.getStartIndex();
        int b = ctx.stop.getStopIndex();
        if(input==null) throw new RuntimeException("Input stream undefined");
        return input.getText(new Interval(a,b));
    }
    @Override
    public Integer visitIf_stat(firstParser.If_statContext ctx) {
        Integer result = 0;
        if (visit(ctx.cond)!=0) {
            result = visit(ctx.then);
        }
        else {
            if(ctx.else_ != null)
                result = visit(ctx.else_);
        }
        return result;
    }

    @Override
    public Integer visitVar_delcaration(firstParser.Var_delcarationContext ctx) {
        var varName = ctx.name.getText();
        symbols.newSymbol(varName);
        return super.visitVar_delcaration(ctx);
    }

    @Override
    public Integer visitPrint_stat(firstParser.Print_statContext ctx) {
        var st = ctx.expr();
        var result = visit(st);

        System.out.printf("| %s = %d |\n", getText(st),  result);

        return result;
    }

    @Override
    public Integer visitInt_tok(firstParser.Int_tokContext ctx) {
        return Integer.valueOf(ctx.INT().getText());
    }

    @Override
    public Integer visitPars(firstParser.ParsContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Integer visitNegate(firstParser.NegateContext ctx) {
        var value = Integer.valueOf(ctx.toNeg.getText());
        var negated = !toBoolean(value);
        return toInt(negated);
    }

    @Override
    public Integer visitVar(firstParser.VarContext ctx) {
        return symbols.getSymbol(ctx.ID().getText());
    }

    @Override
    public Integer visitBinOp(firstParser.BinOpContext ctx) {
        Integer result=0;
        Boolean bl;
        Boolean br;

        switch (ctx.op.getType()) {
            case firstLexer.ADD:
                result = visit(ctx.l) + visit(ctx.r);
                break;
            case firstLexer.SUB:
                result = visit(ctx.l) - visit(ctx.r);
                break;
            case firstLexer.MUL:
                result = visit(ctx.l) * visit(ctx.r);
                break;
            case firstLexer.AND:
                bl = toBoolean(visit(ctx.l));
                br = toBoolean(visit(ctx.r));
                result = toInt(bl && br);
                break;
            case firstLexer.OR:
                bl = toBoolean(visit(ctx.l));
                br = toBoolean(visit(ctx.r));
                result = toInt(bl || br);
                break;

            case firstLexer.EQ:
                result = toInt(Objects.equals(visit(ctx.l), visit(ctx.r)));
                break;

            case firstLexer.NEQ:

                result = toInt(!Objects.equals(visit(ctx.l), visit(ctx.r)));
                break;

            case firstLexer.DIV:
                try {
                    result = visit(ctx.l) / visit(ctx.r);
                } catch (Exception e) {
                    System.err.println("Div by zero");
                    throw new ArithmeticException();
                }
        }

        return result;
    }

    @Override
    public Integer visitAssign(firstParser.AssignContext ctx) {
        var symbolName = ctx.name.getText();
        var symbolValue = visit(ctx.value);
        symbols.setSymbol(symbolName, symbolValue);
        return symbolValue;
    }

    private static Boolean toBoolean(Integer i) {
        if (i != 0 && i != 1) {
            throw new IllegalStateException("Integer must be zero or one!");
        }

        return i == 1;
    }

    private static Integer toInt(Boolean b) {
        if (b) {
            return 1;
        } else  {
            return 0;
        }
    }

}
