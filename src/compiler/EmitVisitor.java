package compiler;

import SymbolTable.FunctionSymbolTable;
import grammar.firstLexer;
import org.antlr.v4.runtime.Token;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import grammar.firstBaseVisitor;
import grammar.firstParser;

import java.util.function.Supplier;

public class EmitVisitor extends firstBaseVisitor<ST> {
    private final STGroup stGroup;

    private Integer counter = 0;

    private final FunctionSymbolTable functionSymbolTable = new FunctionSymbolTable();
    private boolean inLocalFunction = false;
    private String localFunctionName = "";

    public EmitVisitor(STGroup group) {
        super();
        this.stGroup = group;
    }

    @Override
    protected ST defaultResult() {
        return stGroup.getInstanceOf("deflt");
    }

    @Override
    protected ST aggregateResult(ST aggregate, ST nextResult) {
        if(nextResult != null)
            aggregate.add("elem", nextResult);
        return aggregate;
    }

    @Override
    public ST visitDef(firstParser.DefContext ctx) {
        var methodName = ctx.name.getText();
        var params = ctx.par.stream().map(Token::getText).toList();
        functionSymbolTable.addMethod(methodName, params);
        return inFunctionScope(methodName, () -> stGroup
                .getInstanceOf("fundef")
                .add("name", methodName)
                .add("body", visit(ctx.block()))
        );
    }

    @Override
    public ST visitFunctionCall(firstParser.FunctionCallContext ctx) {
        return stGroup
                .getInstanceOf("funcall")
                .add("name", ctx.name.getText())
                .add("pars", ctx.par.stream().map(this::visit).toList().reversed());
    }

    @Override
    public ST visitIf_stat(firstParser.If_statContext ctx) {
        return stGroup
                .getInstanceOf("jezeli")
                .add("ifOp", visit(ctx.cond))
                .add("thenCode", visit(ctx.then))
                .add("elseCode", visit(ctx.else_))
                .add("cnt1", counter++)
                .add("cnt2", counter++);
    }

    @Override
    public ST visitVar_delcaration(firstParser.Var_delcarationContext ctx) {
        ST st = stGroup.getInstanceOf("dek");
        var varName = ctx.name.getText();
        st.add("n", varName);
        return st;
    }

    @Override
    public ST visitInt_tok(firstParser.Int_tokContext ctx) {
        ST st = stGroup.getInstanceOf("int");
        st.add("i",ctx.INT().getText());
        return st;
    }

    @Override
    public ST visitBinOp(firstParser.BinOpContext ctx) {
        ST st = switch (ctx.op.getType()) {
            case firstLexer.ADD -> stGroup.getInstanceOf("dodaj");
            case firstLexer.SUB -> stGroup.getInstanceOf("odejmij");
            case firstLexer.MUL -> stGroup.getInstanceOf("pomnoz");
            case firstLexer.DIV -> stGroup.getInstanceOf("podziel");
            default -> throw new IllegalArgumentException("Illegal type [" + ctx.op.getType() + "]");
        };

        return st.add("p1", visit(ctx.l)).add("p2", visit(ctx.r));
    }

    @Override
    public ST visitAssign(firstParser.AssignContext ctx) {
        var st = stGroup.getInstanceOf("pisz_zmienna");
        var symbolName = ctx.name.getText();

        var opSt = visit(ctx.value);

        return st.add("op", opSt).add("ID", symbolName);
    }

    @Override
    public ST visitVar(firstParser.VarContext ctx) {
        var symbolName = ctx.ID().getText();
        if (inLocalFunction) {
            var st = stGroup.getInstanceOf("czytaj-argument");
            var functionIdx = functionSymbolTable.getArgumentIndex(localFunctionName, symbolName);

            // if not found, assume it is global arg
            if (functionIdx != null) {
                return st.add("idx", functionIdx);
            }
        }

        var st = stGroup.getInstanceOf("czytaj_zmienna");
        return st.add("ID", symbolName);
    }

    private <T> T inFunctionScope(String functionName, Supplier<T> func) {
        localFunctionName = functionName;
        inLocalFunction = true;
        var res = func.get();
        inLocalFunction = false;
        return res;
    }

}
