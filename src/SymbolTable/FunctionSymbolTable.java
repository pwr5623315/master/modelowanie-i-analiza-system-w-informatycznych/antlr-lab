package SymbolTable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FunctionSymbolTable {

    private final Map<String, FunctionArguments> functions;

    public FunctionSymbolTable() {
        this.functions = new HashMap<>();
    }

    public void addMethod(String methodName, List<String> arguments) {
        functions.put(methodName, new FunctionArguments(arguments));
    }

    public Integer getArgumentIndex(String methodName, String argumentName) {
        return forMethod(methodName)
                .getIndex(argumentName);
    }

    private FunctionArguments forMethod(String methodName) {
        if (!functions.containsKey(methodName)) {
            throw new RuntimeException("Cannot find method " + methodName);
        }
        return functions.get(methodName);
    }

    private record FunctionArguments(List<String> arguments) {
        public Integer getIndex(String name) {
            var argIndex = arguments.indexOf(name);
            if (argIndex == -1) {
                return null;
            }
            return argIndex + 1;
        }
    }

}
