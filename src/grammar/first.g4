grammar first;

prog:	(stat|def)* EOF ;

stat: expr #expr_stat
    | IF_kw '(' cond=expr ')' then=block  ('else' else=block)? #if_stat
    | '>' expr #print_stat
    | var=VAR_KW name=ID  #var_delcaration
    ;

def : FUNC name=ID '(' par+=ID (',' par+=ID)* ')' body=block ;

block : stat #block_single
    | '{' block* '}' #block_real
    ;

expr:
        op=NOT toNeg=expr #negate
    |   name=ID '(' par+=expr (',' par+=expr)* ')' #functionCall
    |   l=expr op=(MUL|DIV) r=expr #binOp
    |	l=expr op=(ADD|SUB) r=expr #binOp
    |	l=expr op=AND r=expr #binOp
    |	l=expr op=OR r=expr #binOp
    |   l=expr op=(EQ|NEQ) r=expr #binOp
    |	INT #int_tok
    |   ID #var
    |	'(' expr ')' #pars
    | <assoc=right> name=ID '=' value=expr # assign
    ;

// symbols
IF_kw : 'if' ;

VAR_KW : 'var' ;

DIV : '/' ;

MUL : '*' ;

SUB : '-' ;

ADD : '+' ;

NOT : '!' ;

OR : 'or' ;

AND : 'and' ;

EQ : '==' ;

NEQ : '!=' ;

FUNC : 'function' ;

COMMA : ',' ;

//NEWLINE : [\r\n]+ -> skip;
NEWLINE : [\r\n]+ -> channel(HIDDEN);

//WS : [ \t]+ -> skip ;
WS : [ \t]+ -> channel(HIDDEN) ;

INT     : [0-9]+ ;

ID : [a-zA-Z_][a-zA-Z0-9_]* ;

COMMENT : '/*' .*? '*/' -> channel(HIDDEN) ;
LINE_COMMENT : '//' ~'\n'* '\n' -> channel(HIDDEN) ;